
public class Tree {

	private class Node {
		private int data;
        private Node leftChild;
        private Node rightChild;

		public int getData() {
			return this.data;
		}

		public void setData(int data) {
			this.data = data;
		}

        public Node getLeftChild() {
            return leftChild;
        }

        public void setLeftChild(Node leftChild) {
            this.leftChild = leftChild;
        }

        public Node getRightChild() {
            return rightChild;
        }

        public void setRightChild(Node rightChild) {
            this.rightChild = rightChild;
        }
    }

	//---------------------------
	// Constructors

    private Node root = null;

	public Tree() {

	}
	
	
	//--------------------------
	//Data structure operations	
	/**
	 * Voeg een element toe aan de tree
	 */
	public void insert(int o) {
        Node nodeToBeInserted = new Node();
        nodeToBeInserted.setData(o);

		if (root == null) {
            root = nodeToBeInserted;
        } else {
            Node focussedNode = root;
            Node futureparent = null;

            while (true) {
                futureparent = focussedNode;

                if (futureparent.getData() < o) {
                    // Put it on the left side.
                    focussedNode = focussedNode.getLeftChild();

                    if (focussedNode.getLeftChild() == null) {
                        futureparent.setLeftChild(nodeToBeInserted);
                        return;
                    }

                } else {
                    // Put it on the right side.
                    focussedNode = focussedNode.getRightChild();

                    if (focussedNode.getRightChild() == null) {
                        focussedNode.setRightChild(nodeToBeInserted);
                        return;
                    }

                }
            }
        }
	}
	
	/**
	 * Verwijder een object uit de boom
	 */
	public void remove(int studentId) {
        // Find the parent node.

        Node nodeToBeRemoved = this.root;
        Node parentNode = null;

        while (nodeToBeRemoved.getData() != studentId) {

            if (nodeToBeRemoved.getData() < studentId) {
                // Search the left node.

                parentNode = nodeToBeRemoved;
                nodeToBeRemoved = nodeToBeRemoved.getLeftChild();
            } else {
                // Search the Right node.

                parentNode = nodeToBeRemoved;
                nodeToBeRemoved = nodeToBeRemoved.getRightChild();
            }
        }


        if (nodeToBeRemoved.getLeftChild() == null && nodeToBeRemoved.getRightChild() == null) {
            // Case 1 = Node is a leaf node, just delete the node.
            if (parentNode.getLeftChild().equals(nodeToBeRemoved)) {
                parentNode.setLeftChild(null);
            } else {
                parentNode.setRightChild(null);
            }

            return;
        } else if (nodeToBeRemoved.getLeftChild() != null && nodeToBeRemoved.getRightChild() != null) {
            // Case 3 = Node had 2 children. Get right child and set as node. Get right childs
            // right child and set it as right child of this node.

            parentNode.setRightChild(nodeToBeRemoved.getRightChild());
            return;
        } else {
            // Case 2 = Node has 1 child, set child as the node.
            if (nodeToBeRemoved.getLeftChild() != null) {
                parentNode.setLeftChild(nodeToBeRemoved.getLeftChild());
            } else {
                parentNode.setRightChild(nodeToBeRemoved.getRightChild());
            }

            return;
        }

	}
	
	
	/**
	 * Zoek een studentnummer in de boom en return 
	 * het oject
	 */
	public Object find(int studentId) {

        Node focussedNode = this.root;

        while (focussedNode.getData() != studentId) {

            if (focussedNode.getData() < studentId) {
                // Search the left node.

                focussedNode = focussedNode.getLeftChild();
            } else {
                // Search the Right node.

                focussedNode = focussedNode.getRightChild();
            }

            if (focussedNode == null) {
                return null;
            }

        }

        return focussedNode;
	}

	
	public void balance() {
		
	}

    public boolean isBalanced(Node root) {
        if (root == null)
            return true;

        if (getHeight(root) == -1)
            return false;

        return true;
    }

    public int getHeight(Node root) {
        if (root == null)
            return 0;

        int left = getHeight(root.getLeftChild());
        int right = getHeight(root.getRightChild());

        if (left == -1 || right == -1)
            return -1;

        if (Math.abs(left - right) > 1) {
            return -1;
        }

        return Math.max(left, right) + 1;
    }
	
	
	//------------------------------------------------------
	// Utility methods
	/**
	 * Print de tree In-Order
	 */

    void inOrder (Node root)
    {

        if(root == null) return;

        inOrder(root.getLeftChild());

        System.out.println(root.getData());

        inOrder(root.getRightChild());

    }

	public String toString(){

        return "";
	}	
}
